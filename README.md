# RPG CHARACTERS ASSIGNMENT
## JAVA
[![N|Solid](https://icons.iconarchive.com/icons/tatice/cristal-intense/48/Java-icon.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
## Intro
The program should be able to create four different heroes/characters (Mage,Ranger,Rogue,Warrior). Every different type of character/hero has specific primary (Strength,Dexterity,Intelligence,Vitality) and secondary attributes (Health,Armor rating, Elemental resistance). The characters should be able to level up and update those attributes. The characters should also be able to equip four different slots containing armor for head, body and legs and a weapon. Every character has specific rules for what armor-types and weapons-types that can be equipped. If the rules aren't followed the program will throw an exception.


# ✨Features
## Weapon
There are several types of weapons which exist:
• Axes • Bows • Daggers • Hammers • Staffs • Swords • Wands
## Armor
There are several types of Armor that exist:
• Cloth • Leather • Mail • Plate
## Character stats display
All characters need a way to display their statistics to a character sheet. For this example, a simple string generated by using a StringBuilder is a good solution. This sheet should show:
• Character name • Character level • Strength • Dexterity  • Intelligence • Health • Armor Rating
• Elemental Resistance • DPS
