package com.company.heros;

import com.company.attributes.PrimaryAttributes;
import com.company.attributes.SecondaryAttributes;
import com.company.customExceptions.InvalidAgeException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Characters_Attributes_AndLeve_Test {
  // A character is level 1 when created.
    @Test
    void Characters_Primer_Lave_Test_AssertEquals() {
        Mage mage = new Mage("Atmar");
        int actual = 1;
        int expected = mage.getLevel();
        assertEquals(expected,actual);
    }
    @Test
    void Characters_Primer_Lave_Test_AssertNotEquals() {
        Mage mage = new Mage("Atmar");
        int actual = 2;
        int expected = mage.getLevel();
        assertNotEquals(expected,actual);
    }
    @Test
    void Characters_LaveUp_Test_AssertEquals() {
        Mage mage = new Mage("Atmar");
        mage.levelUp(1);
        assertEquals(mage.getLevel() , 2);
    }
    @Test
    void Characters_LaveUp_Test_AssertNotEquals() {
        Mage mage = new Mage("Atmar");
        mage.levelUp(2);
        assertNotEquals(mage.getLevel() , 2);
    }
    @Test
    void Characters_LaveUp_Test_assertThrows_less_Than_One_throwException()  {
        Mage mage = new Mage("Atmar");
        assertThrows(IllegalArgumentException.class, () -> mage.levelUp(-1));

    }
    @Test
    void Create_CharacterMage_if_Valid_PrimAttributes_true() {
        Mage mage = new Mage("Atmar");
        PrimaryAttributes expected =  new PrimaryAttributes(1,1,8,5);
        PrimaryAttributes actual = mage.getBasePrimaryAttributes();
        assertTrue(actual.isEqual(expected));
    }

    @Test
    void LevelingUp_Test_RangerCharacter_if_PrimAttributes_Updated_true() {
        Ranger ranger = new Ranger("Ranger");
        ranger.levelUp(1);
        PrimaryAttributes expected =  new PrimaryAttributes(2,12,2,10);
        PrimaryAttributes actual = ranger.getBasePrimaryAttributes();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void LevelingUp_Test_RogueCharacter_if_PrimAttributes_Updated_true() {
        Rogue rogue = new Rogue("Rogue");
        rogue.levelUp(1);
        PrimaryAttributes expected =  new PrimaryAttributes(3,10,2,11);
        PrimaryAttributes actual = rogue.getBasePrimaryAttributes();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void LevelingUp_Test_WarriorCharacter_if_PrimAttributes_Updated_true() {
        Warrior warrior = new Warrior("Warrior");
        warrior.levelUp(1);
        PrimaryAttributes expected =  new PrimaryAttributes(8,4,2,15);
        PrimaryAttributes actual = warrior.getBasePrimaryAttributes();
        assertTrue(actual.isEqual(expected));
    }



    @Test
    void LevelingUp_Test_MageCharacter_if_PrimAttributesUpdated_true() {
        Mage mage = new Mage("Mage");
        mage.levelUp(1);
        PrimaryAttributes expected =  new PrimaryAttributes(2,2,13,8);
        PrimaryAttributes actual = mage.getBasePrimaryAttributes();
        assertTrue(actual.isEqual(expected));
    }
    @Test
    void LevelingUp_Test_WarriorCharacter_isSecondaryAttributes_Updated_true() {
       Mage mage = new Mage("Atmar");
        mage.levelUp(1);
        SecondaryAttributes expected = new SecondaryAttributes(80,4,13);
        SecondaryAttributes actual = mage.getSecondaryAttributes();
        assertTrue(actual.isEqual(expected));

    }

}