package com.company.heros;

import com.company.Equipments.Slot;
import com.company.Equipments.Weapon;
import com.company.Equipments.WeaponType;
import com.company.armor.Armor;
import com.company.armor.ArmorType;
import com.company.attributes.PrimaryAttributes;
import com.company.customExceptions.InvalidArmorException;
import com.company.customExceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Items_And_Equipment_Tests {
    @Test
    void EquipWeapon_Test_Warrior_ToHighLevel_ShouldThrowException() {
        Warrior warrior = new Warrior("Warrior");
        Weapon axe = new Weapon("Axe",2, Slot.Weapon, WeaponType.Axe,3,2);
        assertThrows(InvalidWeaponException.class, () -> warrior.setWeapon(axe));
    }
    @Test
    void EquipArmor_Test_Warrior_ToHighLevel_ShouldThrowException() {
        Warrior warrior = new Warrior("Warrior");
        Armor body = new Armor("body",2,Slot.Body, ArmorType.Plate,new PrimaryAttributes(1,1,1,1));
        assertThrows(InvalidArmorException.class, () -> warrior.setArmor(body));
    }
    @Test
    void EquipWeapon_TestWarrior_WrongWeaponType_ShouldThrowException() {
        Warrior warrior = new Warrior("Warrior");
        Weapon bow = new Weapon("bow",1,Slot.Weapon,WeaponType.Bow,3,2);
        assertThrows(InvalidWeaponException.class, () -> warrior.setWeapon(bow));
    }
    @Test
    void EquipArmor_Test_Warrior_WrongArmorType_ShouldThrowException() {
        Warrior warrior = new Warrior("Warrior");
        Armor body = new Armor("body",1,Slot.Body,ArmorType.Cloth,new PrimaryAttributes(1,1,1,1));
        assertThrows(InvalidArmorException.class, () -> warrior.setArmor(body));
    }
    @Test
    void EquipWeapon_Test_Warrior_ValidWeaponType_ShouldPass() throws InvalidWeaponException {
        Warrior warrior = new Warrior("Warrior");
        Weapon sword = new Weapon("sword",1,Slot.Weapon,WeaponType.Sword,3,2);
        assertTrue(warrior.setWeapon(sword) ==true);
    }
    @Test
    void EquipArmor_Test_Warrior_ValidArmorType_ShouldPass() throws InvalidArmorException {
        Warrior warrior = new Warrior("Warrior");
        Armor body = new Armor("body",1,Slot.Body,ArmorType.Mail,new PrimaryAttributes(1,1,1,1));
        assertTrue(warrior.setArmor(body)==true);
    }
    @Test
    void CalculateDPS_Test_if_NoWeaponAttachedProperly_ShouldPass() {
        Warrior warrior = new Warrior("Warrior");
        double expected = 1*(1+(5.0/100));
        assertTrue( expected == warrior.getCharacterDPS());
    }
    @Test
    void CalculateDPS_Test_if_AxeWeaponAttachedProperly_ShouldPass() throws InvalidWeaponException, InvalidArmorException {
        Warrior warrior = new Warrior("Warrior");
        Weapon axe = new Weapon("Axe",1,Slot.Weapon, WeaponType.Axe,7,1.1);
        warrior.setWeapon(axe);
        double expected = (7*1.1) * (1+((5.0)/100));
        assertTrue( expected == warrior.getCharacterDPS());
    }
    @Test
    void CalculateDPS_Test_if_WeaponAndArmorAttachedProperly_ShouldPass() throws InvalidWeaponException, InvalidArmorException {
        Warrior warrior = new Warrior("Warrior");
        Weapon axe = new Weapon("Axe",1,Slot.Weapon,WeaponType.Axe,7,1.1);
        Armor body = new Armor("body",1,Slot.Body,ArmorType.Plate,new PrimaryAttributes(1,0,0,2));
        warrior.setWeapon(axe);
        warrior.setArmor(body);
        double expected = (7*1.1) * (1+((5.0+1)/100));
        assertTrue( expected == warrior.getCharacterDPS());
    }
}
