package com.company.customExceptions;

public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(String message) {
        super(message);
    }
}
