package com.company.customExceptions;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String message) {
        super(message);
    }
}
