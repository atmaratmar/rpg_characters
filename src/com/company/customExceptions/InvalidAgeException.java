package com.company.customExceptions;

public class InvalidAgeException extends Throwable {
    public void validate (int level) throws InvalidAgeException{
        if(level < 1){
            // throw an object of user defined exception
            throw new InvalidAgeException();
        }
    }
}
