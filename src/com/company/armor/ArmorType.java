package com.company.armor;

public enum ArmorType {
    Cloth,
    Leather,
    Mail,
    Plate;
}
