package com.company.armor;

import com.company.Equipments.Item;
import com.company.Equipments.Slot;
import com.company.Equipments.Weapon;
import com.company.attributes.PrimaryAttributes;

public class Armor extends Item {

    private ArmorType armorType;
    private PrimaryAttributes armorAttributes;

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public PrimaryAttributes getArmorAttributes() {
        return armorAttributes;
    }

    public void setArmorAttributes(PrimaryAttributes armorAttributes) {
        this.armorAttributes = armorAttributes;
    }

    public Armor(String name, int level, Slot slot,ArmorType armorType, PrimaryAttributes armorAttributes) {
        super(name, level, slot);
        this.armorType= armorType;
        this.armorAttributes = armorAttributes;
    }

}
