package com.company.attributes;

public class PrimaryAttributes {


    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public PrimaryAttributes(int strength, int dexterity, int intelligence, int vitality) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }
    /*
       isEqual to Test if primary attributes are equal to the expected
     */
    public boolean isEqual(PrimaryAttributes primaryAttributes) {
        if(primaryAttributes instanceof PrimaryAttributes){
            if(this.strength == primaryAttributes.getStrength() &&this.dexterity == primaryAttributes.getDexterity()&&this.intelligence == primaryAttributes.getIntelligence() && this.vitality == primaryAttributes.getVitality()) {
                return true;
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }
    }
}
