package com.company.attributes;

public class SecondaryAttributes {

    private int health =10;
    private int armorRating;
    private int elementalResistance;

    public SecondaryAttributes(int health, int armorRating, int elemantalResistance) {
        this.health = health;
        this.armorRating = armorRating;
        this.elementalResistance = elemantalResistance;

    }
    // Overloaded constructor that takes PrimaryAttributes class object as a parameter
    public SecondaryAttributes( PrimaryAttributes primaryAttributes) {
        this.health= health * primaryAttributes.getVitality();
        this.armorRating = (primaryAttributes.getStrength()+ primaryAttributes.getDexterity());
        this.elementalResistance = primaryAttributes.getIntelligence();
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(PrimaryAttributes primaryAttributes) {
        this.health = 10*primaryAttributes.getVitality();
    }

    public int getArmorRating() {
        return armorRating;
    }

    public void setArmorRating(PrimaryAttributes primaryAttributes) {
        this.armorRating = (primaryAttributes.getStrength()+ primaryAttributes.getDexterity());
    }

    public int getElementalResistance(){
        return  elementalResistance;
    };

    public void setElementalResistance(PrimaryAttributes primaryAttributes) {
        this.elementalResistance= primaryAttributes.getIntelligence();
    }
    public boolean isEqual(SecondaryAttributes secondaryAttributes) {
        if (secondaryAttributes instanceof SecondaryAttributes) {
            if (this.health == secondaryAttributes.getHealth() && this.armorRating == secondaryAttributes.getArmorRating() && this.elementalResistance == secondaryAttributes.getElementalResistance()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
