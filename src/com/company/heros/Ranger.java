package com.company.heros;

import com.company.Equipments.Weapon;
import com.company.Equipments.WeaponType;
import com.company.armor.Armor;
import com.company.armor.ArmorType;
import com.company.attributes.PrimaryAttributes;
import com.company.attributes.SecondaryAttributes;
import com.company.customExceptions.InvalidArmorException;
import com.company.customExceptions.InvalidWeaponException;

public class Ranger extends Characters {

    public Ranger(String name) {
        super(name,1,7,1,8,7);

    }
    // Method for leveling up the character once every time methos is called.
    // Method increases primary and secondary attributes.
    public void levelUp(int levelUp) {
        levelUpHero(new PrimaryAttributes(1,5,1,2),levelUp);
    }


    @Override
    public boolean setWeapon(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getWeaponType() == WeaponType.Bow)
            if(getLevel() < weapon.getLevel()) {
                throw new InvalidWeaponException("Mage characters level is to low for this weapon");
            }
            else {
                setCharacterDPS(weapon.getDPS() * ((1+((double) getTotalPrimaryAttributes().getDexterity()/100))));
                weaponEquipment(weapon);
                super.weapon = weapon;
                return true;
            }
        else{
            throw new InvalidWeaponException("Cant add this weapontype to mage character!");
        }
    }

    public Weapon getWeapon() {
        return weapon;
    }

    @Override
    public Armor getArmor() {
        return super.getArmor();
    }

    @Override

    public boolean setArmor(Armor armor) throws InvalidArmorException {
        if(armor.getArmorType() == ArmorType.Leather || armor.getArmorType() == ArmorType.Mail){
            if (getLevel() < armor.getLevel()){
                throw new InvalidArmorException("Mage characters level is to low for this armor");
            }
            else {
                weaponEquipment(armor);
                super.setArmor(armor);
                updateTotal();
                if(super.getWeapon() != null) {
                    setCharacterDPS(super.getWeapon().getDPS() * ((1 + ((double) getTotalPrimaryAttributes().getDexterity() / 100))));
                }
                else {
                    setCharacterDPS(1 * ((1 + ((double) getTotalPrimaryAttributes().getDexterity() / 100))));
                }
                return true;
            }
        }
        else{
            throw new InvalidArmorException("Cant add this Armortype to a Mage character");
        }
    }
}