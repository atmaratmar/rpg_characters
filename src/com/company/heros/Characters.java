package com.company.heros;

import com.company.Equipments.Item;
import com.company.Equipments.Slot;
import com.company.Equipments.Weapon;
import com.company.armor.Armor;
import com.company.attributes.PrimaryAttributes;
import com.company.attributes.SecondaryAttributes;
import com.company.customExceptions.InvalidArmorException;
import com.company.customExceptions.InvalidWeaponException;

import java.util.HashMap;

public class Characters {
    private String name;
    private int level;
    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;
    private double characterDPS;
    private Armor armor;
    protected Weapon weapon;
    private PrimaryAttributes basePrimaryAttributes;
    private PrimaryAttributes totalPrimaryAttributes;
    private SecondaryAttributes secondaryAttributes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public double getCharacterDPS() {
        return characterDPS;
    }

    public void setCharacterDPS(double characterDPS) {
        this.characterDPS = characterDPS;
    }

    public Armor getArmor() {
        return armor;
    }

    public boolean setArmor(Armor armor) throws InvalidArmorException {
        this.armor = armor;
        return true;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public boolean setWeapon(Weapon weapon) throws InvalidWeaponException {
        this.weapon = weapon;
        return true;
    }



    public void setTotalPrimaryAttributes(PrimaryAttributes totalPrimaryAttributes) {
        this.totalPrimaryAttributes = totalPrimaryAttributes;
    }


    public void setSecondaryAttributes(SecondaryAttributes secondaryAttributes) {
        this.secondaryAttributes = secondaryAttributes;
    }

    public Characters(String name, int strength, int dexterity, int intelligence, int vitality, double primaryAttributes) {
        this.level =1;
        this.name = name;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
        this.basePrimaryAttributes = new PrimaryAttributes(this.strength,this.dexterity,this.intelligence,this.vitality);
        this.totalPrimaryAttributes = new PrimaryAttributes(this.strength,this.dexterity,this.intelligence,this.vitality);
        this.secondaryAttributes = new SecondaryAttributes(this.totalPrimaryAttributes);
        this.characterDPS = ((1 + ((primaryAttributes / 100))));

    }

    HashMap<Slot, Item> equipment = new HashMap<Slot,Item>();

    // Initializes inventory HasMap by creating as many slots as there is for items
    public void setEquipment(HashMap<Slot, Item> equipment) {
        this.equipment = equipment;
    }
    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }


    public void weaponEquipment(Weapon weapon) { this.equipment.put(weapon.getSlot(),weapon); }

    public void weaponEquipment(Armor armor) {
        this.equipment.put(armor.getSlot(), armor);
    }

    public PrimaryAttributes getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        this.basePrimaryAttributes = basePrimaryAttributes;
    }

    public PrimaryAttributes getTotalPrimaryAttributes() {
        return totalPrimaryAttributes;
    }

    public SecondaryAttributes getSecondaryAttributes() {
        return secondaryAttributes;
    }
    /*
      IllegalArgumentException should be thrown when a value is provided to an argument that just doesn’t quite
      work within the business logic of the application (if the value is below 1 then the IllegalArgumentException will
       throw)
    */
    public void levelUpHero(PrimaryAttributes levelUpHero, int level){
        if(level >0) {
            PrimaryAttributes levelUp = levelUpHero;
            for(int i=0; i<level; i++) {
                this.level++;
                basePrimaryAttributes.setStrength(basePrimaryAttributes.getStrength() + levelUp.getStrength());
                basePrimaryAttributes.setDexterity(basePrimaryAttributes.getDexterity() +levelUp.getDexterity());
                basePrimaryAttributes.setIntelligence(basePrimaryAttributes.getIntelligence()+levelUp.getIntelligence());
                basePrimaryAttributes.setVitality((basePrimaryAttributes.getVitality()+levelUp.getVitality()));
                secondaryAttributes.setHealth(basePrimaryAttributes);
                secondaryAttributes.setHealth(basePrimaryAttributes);
                secondaryAttributes.setArmorRating(basePrimaryAttributes);
            }
            updateTotal();
        }
        else {
            throw new IllegalArgumentException("The levelUp needs to be greater then 0");
        }
    }

    public void updateTotal() {
        int strength = basePrimaryAttributes.getStrength();
        int dexterity = basePrimaryAttributes.getDexterity();
        int intelligence = basePrimaryAttributes.getIntelligence();
        int vitality = basePrimaryAttributes.getVitality();
        for(Item item : equipment.values()) {
            if(item instanceof Armor){
                Armor armor = (Armor) item;
                strength += armor.getArmorAttributes().getStrength();
                dexterity += armor.getArmorAttributes().getDexterity();
                intelligence += armor.getArmorAttributes().getIntelligence();
                vitality += armor.getArmorAttributes().getIntelligence();
            }
        }
        this.totalPrimaryAttributes = new PrimaryAttributes(strength,dexterity,intelligence,vitality);
        secondaryAttributes.setHealth(totalPrimaryAttributes);
        secondaryAttributes.setElementalResistance(totalPrimaryAttributes);
        secondaryAttributes.setArmorRating(totalPrimaryAttributes);
    }
}
