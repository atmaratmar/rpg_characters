package com.company.Equipments;

public enum WeaponType {
    Axe,
    Bow,
    Dagger,
    Hammer,
    Staff,
    Sword,
    Wand;
}
